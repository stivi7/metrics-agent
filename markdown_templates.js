const os = require('os');

const getMessageTemplate = (type, usage) => {
  const hostname = os.hostname();

  return (
    `*Hostname:* _${hostname}_ 
    
*Warning:* _${type} usage: ${usage}_`
  )
}

module.exports = getMessageTemplate;