class NotificationCache {
  constructor(cachedTime) {
    this.memoryNotified = false;
    this.cpuNotified = false;

    this.cachedTime = cachedTime;
  }

  setNotified(type, bool) {
    this[type] = bool;

    setTimeout(() => {
      this[type] = false;
    }, this.cachedTime)
  }
}

module.exports = NotificationCache;
