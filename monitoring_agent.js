const os = require('os');

const getMessageTemplate = require('./markdown_templates');
const { RAM, CPU } = require('./constants/monitoring');

class MonitoringAgent {
  constructor(bot, cache, memoryThreshold, cpuThreshold) {
    this.bot = bot;
    this.cache = cache;
    this.monitoringInterval = null;

    this.memoryThreshold = memoryThreshold;
    this.cpuThreshold = cpuThreshold;
  }

  getUsedMemory() {
    const divider = Math.pow(1024, 2)
    const totalMemory = os.totalmem() / divider;
    const freeMemory = os.freemem() / divider;

    return (totalMemory - freeMemory);
  }

  getCpuUsage() {
    const cpus = os.cpus();
    let totalTime = 0;
    let idleTime = 0;
    
    cpus.map((cpu) => {
      const times = cpu.times;

      for (let time in times) {
        totalTime += times[time];
      }

      idleTime += times.idle;
    });
 
    const usedTime = totalTime - idleTime;
    
    return (Math.floor((usedTime * 100) / totalTime))
  }

  monitor() {
    this.monitoringInterval = setInterval(async () => {
      try {
        const memoryUsage = Math.ceil(this.getUsedMemory());
        const cpuUsage = this.getCpuUsage();

        if ((memoryUsage >= this.memoryThreshold) && !this.cache.memoryNotified) {
          await this.bot.notify(getMessageTemplate(RAM, `${memoryUsage} MB`))

          this.cache.setNotified('memoryNotified', true)
        }

        if ((cpuUsage >= this.cpuThreshold) && !this.cache.cpuNotified) {
          await this.bot.notify(getMessageTemplate(CPU, `${cpuUsage} %`))

          this.cache.setNotified('cpuNotified', true)
        }
      } catch (e) {
        console.log('ERROR ==>', error)
      }
    }, 1000);
  }

  stopMonitoring() {
    if (this.monitoringInterval) {
      clearInterval(this.monitoringInterval);
    }
  }
}

module.exports = MonitoringAgent;
