require('dotenv').config()
const { processArgvs } = require('./helpers');

const MonitoringAgent = require('./monitoring_agent');
const NotificationCache = require('./notification_cache');
const NotificationBot = require('./bot');

// Cache time is in seconds
const defaultCacheTime = 300;
const defaultMemoryThreshold = 4000;
const defaultCpuThreshold = 30;

const [
  cacheTime = defaultCacheTime,
  memoryThreshold = defaultMemoryThreshold,
  cpuThreshold = defaultCpuThreshold,
] = processArgvs();


const cache = new NotificationCache(cacheTime * 1000);
const bot = new NotificationBot();
const agent = new MonitoringAgent(bot, cache, memoryThreshold, cpuThreshold);

agent.monitor();