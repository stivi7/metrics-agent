const processArgvs = () => {
  const [,, ...args] = process.argv;

  const [
    cacheTime,
    memoryThreshold,
    cpuThreshold,
  ] = args;

  const formatedArgs = Array(3);

  if (cacheTime && Number(cacheTime)) {
    formatedArgs[0] = Number(cacheTime);
  }

  if (memoryThreshold && Number(memoryThreshold)) {
    formatedArgs[1] = Number(memoryThreshold);
  }

  if (cpuThreshold && Number(cpuThreshold)) {
    formatedArgs[2] = Number(cpuThreshold);
  }

  return formatedArgs;
}

module.exports = {
  processArgvs,
};