const TelegramBot = require('node-telegram-bot-api');

const bot = new TelegramBot(process.env.BOT_TOKEN);
class NotificationBot {
  async getChatsToBeNotified() {
    try {
      const messages = await bot.getUpdates();  
      const chatIds = messages.map(msg => msg.message.chat.id)

      return chatIds.reduce((acc, id) => !acc.includes(id) ? [...acc, id] : acc, []);
    } catch (error) {
      throw error;
    }
  }

  async sendMessage(id, message) {
    try {
      await bot.sendMessage(id, message, { parse_mode: 'Markdown' });
    } catch (error) {
      throw error;
    }
  }

  async notify(message) {
    try {
      const chatIds = await this.getChatsToBeNotified();

      const promises = chatIds.map(chatId => this.sendMessage(chatId, message));

      await Promise.allSettled(promises);
    } catch (error) {
      throw error;
    }
  }
}

module.exports = NotificationBot;